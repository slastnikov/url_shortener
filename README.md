#  URL Shortener
*************

This program make short URLs like goo.gl online tool do.

## Requirements
- This tool needs BCMath (http://php.net/manual/ru/bc.installation.php) PHP extention
- mod_rewrite 
- Input links must be full URLs (with http:// or https://)

********************************************************

BEFORE INSTALLATION
-------------------

If you do not have Composer, you may install it by following the instructions at getcomposer.org.

INSTALLATION
------------

### Install from Bitbucket

Extract the archive file or clone this repository.
```
git clone https://slastnikov@bitbucket.org/slastnikov/url_shortener.git
```

After extraction run
```
composer install
```
---

Initialise the application

```
php install/install.php MYSQL_HOST=dbhost MYSQL_USER=dbuser MYSQL_PASS=dbpass MYSQL_DB=dbname
```

if you want to use command line arguments to provide database connection parameters

or

```
php install/install.php -c
```
if you want to use settings from /engine/config/config.php

This will create a database for the project.




