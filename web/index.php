<?php
/**
 * Url Shortner v.1.0.1
 */
use engine\UrlShortener;

const DEV_EMAIL = "e.slastnikov@gmail.com";
require_once __DIR__ . '/../vendor/autoload.php';

$url_shortener = new UrlShortener();

// starting the application engine

if (!$url_shortener->init()) {
    echo "Internal Error: Please contact the developer <a href='mailto:".DEV_EMAIL."'>".DEV_EMAIL."</a>.";
    die();
}
