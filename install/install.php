#!/usr/bin/php
<?php

$config = [];
foreach ($argv as $arg) {
    if (substr($arg, 0, strlen('MYSQL_HOST=')) == 'MYSQL_HOST=') {
        $config['MYSQL_HOST'] = substr($arg, strlen('MYSQL_HOST='));
        continue;
    }

    if (substr($arg, 0, strlen('MYSQL_USER=')) == 'MYSQL_USER=') {
        $config['MYSQL_USER'] = substr($arg, strlen('MYSQL_USER='));
        continue;
    }

    if (substr($arg, 0, strlen('MYSQL_PASS=')) == 'MYSQL_PASS=') {
        $config['MYSQL_PASS'] = substr($arg, strlen('MYSQL_PASS='));
        continue;
    }

    if (substr($arg, 0, strlen('MYSQL_DB=')) == 'MYSQL_DB=') {
        $config['MYSQL_DB'] = substr($arg, strlen('MYSQL_DB='));
        continue;
    }

    if (substr($arg, 0, strlen('-c')) == '-c') {
        $config = include(__DIR__.'/../engine/config/config.php');
        break;
    }
}

$link = mysqli_connect($config['MYSQL_HOST'], $config['MYSQL_USER'], $config['MYSQL_PASS']) or die('Could not connect to database... ');
if ( !mysqli_query($link, 'CREATE DATABASE ' . $config['MYSQL_DB']) ) {
    echo $config['MYSQL_DB'] . ' already exists or an error happened... ';
}

mysqli_select_db($link, $config['MYSQL_DB']) or die('Could not select database... ');

$query = 'CREATE TABLE `'.$config['MYSQL_DB'].'`.`short_urls` ( `id` SERIAL NOT NULL AUTO_INCREMENT , `url` TEXT NOT NULL , `created_at` TIMESTAMP NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM AUTO_INCREMENT=20000 CHARACTER SET utf8 COLLATE utf8_general_ci;';

if (mysqli_query($link, $query)) {
    echo 'Table "short_urls" successfully created';
}
else {
    echo 'Couldn\'t create table';
};

?>