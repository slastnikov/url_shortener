<?php
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="styles/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="styles/style.css" />

    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/jquery.min.js"></script>
    <script>
        $(document).ready( function() {
            $('#sbmt').click(function(){
                var longUrl = $('#url').val();
                $.ajax({
                    method: 'POST',
                    dataType: 'json',
                    timeout: 1000,
                    url: '',
                    data: {
                        url: longUrl
                    },
                    success: function(data) {
                        if (data.status == 0) {
                            $('#result').removeClass();
                            $('#result').addClass('bg-danger');
                            $('#result').html('<i>Error: </i>' + data.message);
                        }
                        else {
                            $('#result').removeClass();
                            $('#result').addClass('bg-success');
                            $('#result').html('<i>Your short URL: </i><a href="' + data.message + '">' + data.message + '</a>');
                        }
                    },
                    error: function () {
                        $('#result').removeClass();
                        $('#result').addClass('bg-danger');
                        $('#result').html('<i>Error: </i> Upsss!.. Something wrong');
                    }
                });
                return false;
            });
        });
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-2 col-md-4"></div>
        <div class="col-xs-8 col-md-4 center-block">
            <h1>URL Shortener</h1>
            <p>
                Paste your long URL here:
            </p>
        </div>
        <div class="col-xs-2 col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-xs-2 col-md-3"></div>
        <div class="col-xs-8 col-md-6 center-block">
            <form id="url-form" role="form">
                <div class="form-group">
                    <input type="text" class="form-control" id="url" placeholder="http://www.example.dom">
                </div>

                <button type="submit" class="btn btn-default" id="sbmt">Send</button>
            </form>
            <p id="result">
            </p>
        </div>
        <div class="col-xs-2 col-md-3"></div>
    </div>
</div>
</body>
</html>
