<html>
<head>
    <link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="styles/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="styles/style.css" />
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-2 col-md-4"></div>
        <div class="col-xs-8 col-md-4 center-block">
            <h1>URL Shortener</h1>
            <p>
                Ohh... it seems like you have a wrong short address. Maybe you would try to make a new one?
            </p>
        </div>
        <div class="col-xs-2 col-md-4"></div>
    </div>
    <div class="row">
        <div class="col-xs-2 col-md-3"></div>
        <div class="col-xs-8 col-md-6 center-block">
            <a href="<?= $this->base_url ?>">URL Shortener home page</a>
        </div>
        <div class="col-xs-2 col-md-3"></div>
    </div>
</div>
</body>
</html>
