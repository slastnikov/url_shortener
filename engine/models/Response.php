<?php
/**
 * Response class encapsulates response functions
 */

namespace engine\models;


class Response
{
    public $type;
    private $base_url;

    public function __construct($config)
    {
        $this->base_url = $config['BASE_URL'];
    }

    public function sendAjax($params) {
        if (is_array($params)) {
            echo json_encode($params);
        }
        else {
            echo json_encode(['status' => 0, 'message' => 'internal error']);
            return false;
        }

    }

    /**
     * @param string $filename
     */
    public function render($filename) {
        $path = null;
        if ( file_exists($path = __DIR__.'/../views/'.$filename.'.php') ) {
            ob_start();
            require($path);
            ob_flush();
        }

    }

    public function redirect($url) {
        if ($url)
            header( 'Location: ' . $url, true );
        else
            $this->render('error');
    }
}