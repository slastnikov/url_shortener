<?php

namespace engine\models;


class Database
{
    private $database;

    /**
     * Database constructor.
     * @param $config
     */
    public function __construct($config)
    {
        require __DIR__.'/../../vendor/autoload.php';

        if (is_array($config)) {
            $this->database = new \medoo([
                'database_type' => 'mysql',
                'database_name' => $config['MYSQL_DB'],
                'server' => $config['MYSQL_HOST'],
                'username' => $config['MYSQL_USER'],
                'password' => $config['MYSQL_PASS'],
                'charset' => 'utf8',
            ]);

            return true;
        }
        return false;
    }

    /**
     * @param $str
     * @return string
     */
    public function prepare_string($str) {
        return trim(addslashes($str));
    }

    /**
     * @param $fields
     * @param $from
     * @param null $where
     * @return array|bool|null
     * @throws Exception
     */
    public function select ($fields, $from, $where = null) {

        $res = $this->database->select($from, $fields, $where);
        if (!is_array($res)) return false;

        return $res;
    }

    /**
     * @param $table
     * @param $fields
     * @return array
     * @throws Exception
     */
    public function insert($table, $fields) {
        if (!($table || $fields)) {
            throw new Exception('Wrong parameters in insert statement');
        }

        return $this->database->insert($table, $fields);
    }

    public function delete($table, $where) {
        if (!($table )) {
            throw new Exception('Wrong parameters in delete statement');
        }

        return $this->database->delete($table, $where);
    }
}