<?php

namespace engine\models;

class ShortURL
{
    private $db;

    public function __construct($config) {
        $this->db = $config['database'];
    }

    public function getShortUrl($long_url) {
        if ($id = $this->add_url($long_url)) {
            return $this->convert_to_url($id);
        }

        return false;
    }

    public function getLongUrl($short_url) {
        $id = $this->anyToDec($short_url);

        $res = $this->db->select(['url'], 'short_urls', ['id' => $id]);

        if ( $res )
        {
            return $res[0]['url'];
        }
        else
        {
            return false;
        }
    }

    private function add_url($url)
    {
        // check if the url is already in the DB
        if ( $id = $this->get_id($url) )
        {
            return $id;
        }
        else // otherwise, put it in
        {
            return $this->db->insert('short_urls', ['url' => $url, 'created_at' => 'NOW()']);
        }
    }

    private function get_id($url) {

        $res = $this->db->select(['id'], 'short_urls', ['url' => $url]);

        if ( $res )
        {
            return $res[0]['id'];
        }
        else
        {
            return false;
        }
    }

    private function convert_to_url($id) {
        return $this->decToAny($id);
    }

    private function decToAny( $num, $base = null, $index = null ) {

        if ( $num <= 0 ) return false;

        if ( !$index )
            $index = '0123456789abcdefghijklmnopqrstuvwxyz';

        if ( !$base )
            $base = strlen( $index );
        else
            $index = substr( $index, 0, $base );

        $res = '';

        while( $num > 0 ) {
            $char = bcmod( $num, $base );
            $res = substr( $index, $char, 1 ) . $res;
            $num = bcsub( $num, $char );
            $num = bcdiv( $num, $base );
        }

        return $res;
    }

    private function anyToDec( $num, $base = null, $index = null ) {

        if ( !$index )
            $index = '0123456789abcdefghijklmnopqrstuvwxyz';

        if ( !$base ) {
            $base = strlen( $index );
        }

        $out = 0;
        $len = strlen( $num ) - 1;

        for ( $t = 0; $t <= $len; $t++ ) {
            $out = $out + strpos( $index, substr( $num, $t, 1 ) ) * pow( $base, $len - $t );
        }
        return $out;
    }
}