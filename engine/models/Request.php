<?php
/**
 * Request class encapsulates request functions
 */

namespace engine\models;

class Request
{
    /**
     * detects whether the request has "HTTP_X_REQUESTED_WITH" header
     */
    public function isAjax() {
        if ( isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
        ) {
            return true;
        }

        return false;
    }

    public function post($var_name=false) {

        if (!$var_name) {
            return $_POST;
        }

        if (isset($_POST[$var_name])) {
            return $_POST[$var_name];
        }

        return false;
    }

    public function getURI() {
        return $_SERVER['REQUEST_URI'];
    }
}