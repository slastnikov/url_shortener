<?php
/**
 * Date: 01.03.2016
 * Time: 23:38
 */

namespace engine\controllers;

use Controller;

class DefaultController extends Controller
{
    public function defaultAction() {
        $this->engine->response->render('index');
    }

    public function redirectAction($short_url) {
        $this->engine->response->redirect($this->engine->shortener->getLongUrl($short_url));
    }
}