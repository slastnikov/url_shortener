<?php
/**
 * ajaxController class uses lilUrl (http://lilurl.sourceforge.net/ released as Free software,
 * under the GNU general public license).
 */

namespace engine\contollers;

use Controller;

class AjaxController extends Controller
{
    /**
     * By default ajaxController encodes input string and sends it back to user
     */
    public function defaultAction() {

        $long_url = $this->engine->db->prepare_string($this->engine->request->post('url'));

        if (!filter_var($long_url, FILTER_VALIDATE_URL))
            return $this->engine->response->sendAjax(['status' => 0, 'message' => 'this URL can\'t be shorten']);

        $res = $this->engine->shortener->getShortUrl($long_url);

        if ($res)
            return $this->engine->response->sendAjax(['status' => 1, 'message' => $this->engine->config['BASE_URL'] . '/' . $res]);
        else
            return $this->engine->response->sendAjax(['status' => 0, 'message' => 'this URL can\'t be shorten']);
    }
}