<?php

/**
 * UrlShortner is the main engine class
 */

namespace engine;

use engine\contollers\AjaxController;
use engine\controllers\DefaultController;
use engine\models\Database;
use engine\models\Request;
use engine\models\Response;
use Composer\Script\Event;
use engine\models\ShortURL;

class UrlShortener
{
    public $request;
    public $response;
    public $db;
    public $config;
    public $shortener;

    /**
     * Initialize the engine
     */
    public function __construct() {

        $this->config = include(__DIR__.'/config/config.php');

        $this->request = new Request();
        $this->response = new Response(['BASE_URL' => $this->config['BASE_URL']]);
        $this->db = new Database($this->config);
        $this->shortener = new ShortURL(['database' => $this->db]);
    }

    /**
     * The main process
     */
    public function init() {

        if ($this->request->isAjax()) {
            $ajax_controller = new AjaxController($this);
            $ajax_controller->defaultAction();
        }
        else {
            $default_controller = new DefaultController($this);
            $uri = $this->request->getURI();
            if ($uri == '/') {
                $default_controller->defaultAction();
            }
            else {
                $default_controller->redirectAction(filter_var($uri, FILTER_SANITIZE_EMAIL));
            }


        }

        return true;
    }

    public static function assetsUpdate(Event $event) {
        copy(__DIR__.'/../vendor/components/bootstrap/css/bootstrap.min.css', __DIR__.'/../web/styles/bootstrap.min.css');
        copy(__DIR__.'/../vendor/components/bootstrap/css/bootstrap-theme.min.css', __DIR__.'/../web/styles/bootstrap-theme.min.css');

        copy(__DIR__.'/../vendor/components/jquery/jquery.min.js', __DIR__.'/../web/scripts/jquery.min.js');
        copy(__DIR__.'/../vendor/components/bootstrap/js/bootstrap.min.js', __DIR__.'/../web/scripts/bootstrap.min.js');
    }
}