<?php
/**
 * base Controller class
 */
use engine\UrlShortener;

class Controller
{
    public $engine;

    /**
     * Controller constructor.
     * @param engine\UrlShortener $url_shortener
     */
    public function __construct(UrlShortener $url_shortener) {
        $this->engine = $url_shortener;
    }

}