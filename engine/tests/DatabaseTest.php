<?php

namespace engine\tests;

use engine\models\Database;

class DatabaseTest extends \PHPUnit_Framework_TestCase
{
    public $db;
    public $config;

    public function setUp() {
        $this->config = include(__DIR__.'/../config/config.php');
        $this->db = new Database($this->config);
    }

    protected function tearDown()
    {
        $this->db = null;
    }

    public function testPrepareString() {
        $str = '   http://sample.com   ';
        $this->assertEquals('http://sample.com', $this->db->prepare_string($str));

        $str = 'http://sample.com"\'\n';
        $this->assertEquals('http://sample.com\"\\\'\\\n', $this->db->prepare_string($str));
    }

    public function testCRD() {
        $url = 'http://www.ya.ru';
        $id = $this->db->insert('short_urls', ['url' => $url, 'created_at' => 'NOW()']);

        $this->assertInternalType('int', (int)$id);

        $res = $this->db->select(['url'], 'short_urls', ['id' => $id]);

        $this->assertEquals($url, $res[0]['url']);

        $this->db->delete('short_urls', ['id' => $id]);
    }
}